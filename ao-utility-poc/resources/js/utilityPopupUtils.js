var utilityPopupUtils = {

//	openApplicationPopup: function() {
//			
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::datagridhtmltemplatesapp',
//				size : {width: 545, height:550},
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openNewPopupApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::editablecomboapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: false,
//				maxWidth: 400,
//				maxHeight: 200,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'openNewPopupAppId'
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},	
//		
//		openContainerwithData: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::refreshdataconfigapp',
//				solutionId: 'com::apporchid::samples::samples',
//				title: 'Show Container with data',
//				relativePosition : {
//					relativeCompId: 'showDataConfigButtonId'
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openEditableComboPopupApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::editablecombosampleapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: false,
//				maxWidth: 400,
//				maxHeight: 200,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'openEditableComboButtonId'
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//	openFormContainer : function() {
//			global.dynamicFormAppData = {};
//			global.dynamicFormAppData.refererUid = 'com::apporchid::system::testformdynamic';
//			global.dynamicFormAppData.domainId = 'com.apporchid';
//			var referUIdcriteria = window['criteriaUtil'].createCriteria("referer_uid", "Equals", "String", global.dynamicFormAppData.refererUid);
//			criteria = {
//				microAppCriterias : {
//					'formElementTableId' : referUIdcriteria
//				}
//			};
//	        var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::formbuttons',
//				size : {width: 745, height:550},
//				criteria: criteria
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//	viewMoreIconTemplate: function () {
//		return `<button class="menuOptions" style="height:30px;width:70px;background: #3399ff;color: #ffffff;">Edit</button>`;
//	},
//		
//		
//	 openUIContainerMicroapp: function() {
//			
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::uicontainerMicroapp',
//				solutionId: 'com::apporchid::samples::samples',
//				size : {width: 545, height:550},
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openAdvisorySearchApp: function () {
//			window["eventsUtil"].subscribe("advisorySubmitEvent",(event) => {
//		        window["eventsUtil"].unsubscribe('advisorySubmitEvent');
//	            console.log(event.data);
//		    });
//			
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::advisorysearchapp',
//				solutionId: 'com::apporchid::samples::samples',
//				css: 'ao-vux-entity-search-popup',
//				size : {width: 690, height:480},
//				keyValueMap:{
//					evenName:"advisorySubmitEvent"
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openElementSearchApp: function () {
//			window["eventsUtil"].subscribe("entitySubmitEvent",(event) => {
//		        window["eventsUtil"].unsubscribe('entitySubmitEvent');
//	            console.log(event.data);
//		    });
//			
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::elementsearchapp',
//				solutionId: 'com::apporchid::samples::samples',
//				css: 'ao-vux-entity-search-popup',
//				size : {width: 690, height:480},
//				keyValueMap:{
//					eventName:"entitySubmitEvent"
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		onElementFilterSearch(microapp, data) {
//	       let params = {
//	 	           	searchQuery: 'excel',
//	 	           	type: 'PipelineModel'
//	 	        };
//	        microapp.refreshData(params);
//	    },
//	    
//		closeWindowapp: function() {
//	    	$$('com::apporchid::samples::popupreadonlyformappdetailvux-popup').close();
//	    },
//	    
//	    templateJSFunction: function(obj) {
//	    	return `<div class="ao-vux-entity-search">
//            <div class="radio-button">
//                <div class="radio-inner-circle"></div>
//                <div class="radio-outer-circle"></div>
//            </div>
//            <div class="search-content">
//                <div class="search-content-title">${obj.uuid}</div>
//                ${obj.description ? `<div class="search-content-description">${obj.description}</div>` : ""}
//                <div class="search-content-location">${obj.name}</div>
//                <div class="search-content-modified">2020</div>
//                <div class="search-content-tags"></div>
//            </div>
//        </div>`;
//	    },
//	    
//	   onAdvisoryFilterSearch(microapp, data) {
//	       let criteria1 = window['criteriaUtil'].createCriteria('uuid', 'Equals', 'Numeric', data.searchValue);
//	       let criteria2 = window['criteriaUtil'].createCriteria('name', 'Equals', 'String', data.searchValue);
//	       let criteria3 = window['criteriaUtil'].createCriteria('description', 'Equals', 'String', data.searchValue);
//	       let criterias = {criteria1, criteria2, criteria3};
//	       let criteria = CriteriaFactory.createCompositeCriteria('Or', criterias);
//	         let params = {
//	            "criteria": criteria
//	        };	         
//	        microapp.refreshData(params);
//	    },
//	    
//	    openWindowWithoutHeader: function() {
//			var layout = $$('openWindowWithoutHeaderId').$view.getBoundingClientRect();
//			
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::popupreadonlyformapp',
//				position: 'absolute',
//				size: {
//						top: layout.top+38,
//						left: layout.left,
//						width: 300,
//						height: 350,
//					},
//				showHeader: false,
//				asModalWindow: false,
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openWindowApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::readonlyformapp',
//				solutionId: 'com::apporchid::samples::samples',
//				maxWidth: 350,
//				maxHeight: 292,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'openWindowAppId',
//					y: 50,
//					x: 50
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		articleDataviewApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::system::articledataviewapp',
//				solutionId: 'com::apporchid::samples::samples',
//				maxWidth: 350,
//				maxHeight: 292,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'articleDataviewAppId',
//					y: 50,
//					x: 50
//				},
//				
//				keyValueMap: {
//					pipelineId: 'com::apporchid::samples::SampleFieldExpressionPipeline'
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		openPopupApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::readonlyformapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: true,
//				maxWidth: 350,
//				maxHeight: 292,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'openPopupAppId'
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		
//		openPopupResizeApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::readonlyformapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: true,
//				maxWidth: 350,
//				maxHeight: 292,
//				minWidth: 150,
//				minHeight: 150,
//				resize: true,
//				relativePosition : {
//					relativeCompId: 'openPopupResizeAppId',
//					y: 50,
//					x: 50
//				}
//			}
//			appUtil.openApplication(appConfig);
//		},
//		
//		
//		openPopupPaddingTopApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::readonlyformapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: true,
//				maxWidth: 350,
//				maxHeight: 292,
//				minWidth: 150,
//				minHeight: 150,
//				relativePosition : {
//					relativeCompId: 'openPopupPaddingTopAppId',
//					y: 20
//				}
//			}
//
//			appUtil.openApplication(appConfig);
//		},
//		
//		openPopupTreeApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::treepopupapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: true,
//				width: 200,
//				height: 200,
//				maxWidth: 300,
//				maxHeight: 300,
//				minWidth: 100,
//				minHeight: 100,
//				resize: true,
//				relativePosition : {
//					relativeCompId: 'openPopupTreeAppId',
//					y:10
//				}
//			}
//
//			appUtil.openApplication(appConfig);
//		},
//		
//		openPopupTreeFilterApp: function () {
//			var appUtil = new window['AppUtil']();
//			var appConfig = {
//				id: 'com::apporchid::samples::treefilterpopupapp',
//				solutionId: 'com::apporchid::samples::samples',
//				asPopup: true,
//				width: 200,
//				height: 200,
//				maxWidth: 300,
//				maxHeight: 300,
//				minWidth: 100,
//				minHeight: 100,
//				resize: true,
//				relativePosition : {
//					relativeCompId: 'openPopupTreeFilterAppId',
//					y:10
//				}
//			}
//
//			appUtil.openApplication(appConfig);
//		},
//
//	getNotificationDetailTemplate(obj) {
//		var cssClass = 'notification_item';
//		if (obj.isRead) {
//			cssClass += ' read';
//		}
//		if (obj.priority) {
//			cssClass = cssClass + ' ' + obj.priority;
//		}
//		let image = '';
//		if (obj.imageURL) {
//			image = "<div class='image'><img src='" + obj.imageURL + "'/></div>"
//		}
//		let date = obj.date ?  obj.date + " | " : "";
//		let name = obj.name ?  obj.name + " | " : "";
//		let url = obj.url ?  obj.url + " | " : "";
//		let duration = obj.duration ?  "<div class='time_icon vuxicon vuxicon-time-regular'></div><div class='vux_h9 notificationitem_value'><div>" + obj.duration + "</div></div>" : "";
//		return "<div class='" + cssClass + "'>"+ image +"<div class='notificationitem_details'><div class='vux_h7 notification_item_title'><div>" + obj.title + "</div></div><div class='notificationitem_time'><div class='other_details'>" + date + name + url + " </div>"+duration+" <div class='vux_h8 notificationitem_description'><div>" + obj.notificationContent + "</div></div></div></div><span class='more_icon vuxicon vuxicon-more-solid'></span></div>"
//	},
//	
//	getNotificationTemplate(obj) {
//		var cssClass = 'notification_item';
//		if (obj.isRead) {
//			cssClass += ' read';
//		}
//		if (obj.priority) {
//			cssClass = cssClass + ' ' + obj.priority.toLowerCase();
//		}
//		var durationTime = (obj.duration)? "<div class='time_icon vuxicon vuxicon-time-regular'></div><div class='vux_h9 notificationitem_value'><div>" + obj.duration + "</div></div>": "";
//		return "<div class='" + cssClass + "'><div class='vux_h8 notification_item_title'><div>" + obj.title + "</div></div><div class='notificationitem_time'>" + durationTime + "</div><span class='more_icon vuxicon vuxicon-more-solid'></span></div>"
//	},
//	
//	treeItemSelect: function(data) {
//		console.log('tree item selected ' + data);
//	},
//	
//	filemanagerItemSelect: function(data) {
//		console.log('filemanager item selected ' + data);
//	},
//
//	onUpload: function(microAppConfig, response) {
//		$$('com::apporchid::samples::filemanagerapp$fileManagerNewId').getUploader().fileDialog();
//	},

	openFavoriteApp: function() {
		console.log('custom profile menu item clicked');
	},
	
//	toolbarItemClick: function(obj) {
//        console.log('you click on toolbar more',obj);
//    },
//    
//    openPDF: function() {
//		var pdfUtil = new window['PdfUtil']();
//		var pdfConfig = {
//			title: 'Sample PDF',
//		    pageNumber: 1,
//			documentUrl: "/main-ui/solutions/samples/pdf/sample.pdf",
//		}
//		
//		pdfUtil.openPdf(pdfConfig);
//	},
//	
//	openOtherSolutionPageForReferer: function() {
//		var solutionPageUtil = window['SolutionPageUtil'];
//		var referer = {solutionId: "com::apporchid::samples::samples", appId: "apppopupapp"};
//		
//		solutionPageUtil.openPage('trainingPage', 'com::apporchid::training::training', null, null, false,null,null,null,referer);
//	},
//	
//	openOtherSolutionAppForReferer: function() {
//		var referer = {solutionId: "com::apporchid::samples::samples", pageId: "appPopupsPage", appId: "apppopupapp", microAppId: "openOtherSolutionappButton"};
//		
//		var appUtil = new window['AppUtil']();
//		var appConfig = {
//			id: 'com::apporchid::training::exercise1app',
//			solutionId: 'com::apporchid::training::training',
//		}
//
//		appUtil.openApplication(appConfig, referer);
//	},
//	openAccordian: function () {
//		var appUtil = new window['AppUtil']();
//		
//		var appConfig = {
//			id: 'com::apporchid::samples::accordionSingleObjectApp',
//			solutionId: 'com::apporchid::samples::samples',
//			position:'center',
//			size:{
//				width:window.innerWidth*.5,
//				height:window.innerHeight*.8
//			}
//		}
//		appUtil.openApplication(appConfig);
//	},
//
//	openPdfWindowApp: function () {
//        var appUtil = new window['AppUtil']();
//        var appConfig = {
//            id: 'com::apporchid::samples::pdfviewerapp',
//            solutionId: 'com::apporchid::samples::samples',
//            maxWidth: 350,
//            maxHeight: 292,
//            minWidth: 150,
//            minHeight: 150,
//            relativePosition : {
//                relativeCompId: 'openWindowAppId',
//                y: 50,
//                x: 50
//            }
//        }
//        appUtil.openApplication(appConfig);
//    },
//    
//    onBrowseButtonClick: function () {
//    	samplesPopupUtils.openFilemanager("fileSelectTextId");
//	},
//	
//	openFilemanager:function(componentId,showAbsolutePath=true,id){
//		var appUtil = new window['AppUtil']();
//		var appConfig = {
//			id: id || 'com::apporchid::samples::filemanagerfilesystemapp',
//			replaceTitle: true,
//			title: 'FILEMANAGER - DATA FROM FILESYSTEM',
//			componentId: componentId,
//            showAbsolutePath: showAbsolutePath
//		}
//		if (componentId) {
//            appConfig["keyValueMap"] = { componentId: componentId, isAbsolute: false };
//        }
//		appUtil.openApplication(appConfig);
//	},
//
//	onItemDblClick: function (id, e, node) {
//		let filemanager = $$('com::apporchid::samples::filemanagerfilesystemappdetail$fileManagerFilesystemId');
//		let item = filemanager.getItem(id);
//		if (item.type !== "folder") {
//			let filemanagerwindow = $$("com::apporchid::samples::filemanagerfilesystemappdetailvux-popup");
//			let componentId = filemanagerwindow.config.body.componentId;
//			let showAbsolutePath = filemanagerwindow.config.body.showAbsolutePath;
//			let paths = filemanager.getPathNames().map(item => item.value).join('\\');
//			let currentPath = `${paths}\\${item.value}`;
//			let componentValue = showAbsolutePath ? currentPath : item.value;
//			$$(componentId).setValue(componentValue);
//			webix.delay(filemanagerwindow.close, filemanagerwindow);
//		}
//	},
//	
//	openSqlEditor: function(){
//		VuxUtil.openSQLEditor("onSQLEditorApply", null , "Query", "com::apporchid::samples::SqlEditorDataPipeline");
//
//	},
//	
//	openSmartSearchApplication : function() {
//		var appUtil = new window['AppUtil']();
//        var appConfig = {
//            id: 'com::apporchid::system::searchqueryandhistoryapp',
//            solutionId: 'com::apporchid::samples::samples',
//            size : {width: 750, height:550}
//        }
//        appUtil.openApplication(appConfig);
//	},
//	
//	tagTemplateFunction : function(data) {
//	// need to add template
//		return data;
//		console.log(data);		
//	},
//	
//	bodyTemplateConfig : function(data) {
//		// need to add template
//			console.log(data);
//	},
//	
//	openPythonEditor: function(){
//		VuxUtil.openPythonEditor("onPythonEditorApply", null , "Script", "com::apporchid::samples::PythonEditorDataPipeline");
//	},
//	
//	onSubmitClick: function(){
//		
//		if ($$(VuxUtil.getUID("editorSqlMicroAppContainerdetail$aceeditorid")) && $$(VuxUtil.getUID("editorSqlMicroAppContainerdetail$aceeditorid")).getEditorBaseView()) {
//			console.log($$(VuxUtil.getUID("editorSqlMicroAppContainerdetail$aceeditorid")).getEditorBaseView().getValue());
//		}
//	},
//	
//	onCancelClick: function(){
//
//		if ($$(VuxUtil.getUID("editorSqlMicroAppContainerdetailvux-popup")))
//			$$(VuxUtil.getUID("editorSqlMicroAppContainerdetailvux-popup")).close();
//	
//	},
//	
//	onPythonSubmitClick: function(){
//		
//		if ($$(VuxUtil.getUID("editorPythonMicroAppContainerdetail$aceeditorid")) && $$(VuxUtil.getUID("editorPythonMicroAppContainerdetail$aceeditorid")).getEditorBaseView()) {
//			console.log($$(VuxUtil.getUID("editorPythonMicroAppContainerdetail$aceeditorid")).getEditorBaseView().getValue());
//		}
//	},
//	
//	onPythonCancelClick: function(){
//
//		if ($$(VuxUtil.getUID("editorPythonMicroAppContainerdetailvux-popup")))
//			$$(VuxUtil.getUID("editorPythonMicroAppContainerdetailvux-popup")).close();
//	
//	},
//	
//	advisoryWindowError: function () {
//	    try {
//	        var value = 123;
//	        value.split('');
//	    } catch (e) {
//	        // we can directly show the error message or
//	        // we can also pass our own message and detail message like below
//	        //advisoryUtil.showErrorMessage("invalid split","value not exists");
//	        advisoryUtil.showErrorMessage(e);
//	    }
//	},
//	
//	openAppWithCriteria: function () {
//        var appUtil = new window['AppUtil']();
//        var tableCriteria = window['criteriaUtil'].createCriteria('type', 'Equals','Numeric',1);
//        var chartCriteria = window['criteriaUtil'].createCriteria('callType', 'Equals','Numeric',2);
//        var criteria = {microAppCriterias: { 'tableCriteria': tableCriteria, 'chartCriteria':chartCriteria}};
//        var appConfig = {
//            id: 'com::apporchid::samples::appwithcriteria',
//            solutionId: 'com::apporchid::samples::samples',
//            criteria : criteria,
//            title: 'Application with criteria',
//            replaceTitle: true,
//            maxWidth: 350,
//            maxHeight: 292,
//            minWidth: 150,
//            minHeight: 150,
//            relativePosition : {
//                relativeCompId: 'openAppCriteriaId',
//                y: 50,
//                x: 50
//            }
//        }
//        appUtil.openApplication(appConfig);
//    },
//    fileManagerOnFileUpload: function(){
//    	$$("com::apporchid::samples::filemanagerapp$fileManagerNewId").getParentView().renderer.refreshData()
//	},
//	
//	comboTemplate: function (obj) {
//		return `<div>
//					<span> ${obj.id}</span>
//					<span>[${obj.value}]</span>
//				</div>`    	
//	},
//    
//    openFormWithRadioBtn: function () {
//        var appUtil = new window['AppUtil']();
//        var appConfig = {
//            id: 'com::apporchid::samples::formwithradiobutton',
//            solutionId: 'com::apporchid::samples::samples',
//            title: 'Form Custom Validation',
//            replaceTitle: true,
//            size : {width: 650, height:500},
//      
//        }
//        appUtil.openApplication(appConfig);
//    },
//    
//    openAvatarApplication: function () {
//    	var appUtil = new window['AppUtil']();
//        var appConfig = {
//            id: VuxUtil.getUID('avatarApp'),
//            solutionId: 'com::apporchid::samples::samples',
//            css: 'ao-vux-avatar-app',
//            size : {width: 360, height:230}
//        }
//        appUtil.openApplication(appConfig);
//    },
//    
//    openOnBeforeRefreshApp: function () {
//    	var appUtil = new window['AppUtil']();
//        var appConfig = {
//            id: VuxUtil.getUID('onbeforerefresh'),
//            solutionId: 'com::apporchid::samples::samples',
//            title: 'On Before Refresh',
//            replaceTitle: true,
//            size : {width: 700, height:500}
//        }
//        appUtil.openApplication(appConfig);
//    },
//    
//    onFormVariablePipelineChange: function (data, event, app,newValue,oldValue,id) {
//    	var key = $$("com::apporchid::samples::form Variable Micro$comboConfigId").getValue();
//    	if(key){
//    		VuxUtil.showUIContainer($$('dynamicFormVariableContainerId'),{keyValueMap: { "refererUid": data }});
//    	}else{
//    		$$('dynamicFormVariableContainerId').hide();
//    	}
//    	if ($$("formVariableId") && $$("formVariableId").isRendered) {
//			var microApp = MicroAppUtil.getRendererReference('formVariableId','com::apporchid::system::formVariableId');
//			microApp && microApp.refresh({ "refererUid": data })
//		}
//       },
//    
//    onBefroreRefreshFn: function(){
//    	var comboValue = $$(VuxUtil.getUID('onbeforerefreshdetail$combo')).getValue();
//    	var id = comboValue ? comboValue : null;
//    	var criterias = window['criteriaUtil'].createCriteria("deviceId", 'Equals', 'Numeric', id);
//    	return {criteria:criterias}
//    },
//    
//    openMsoRepeater: function(){
//    	var appUtil = new window['AppUtil']();
//		var appConfig = {
//			id: 'com::apporchid::samples::msorepeater',
//			solutionId: 'com::apporchid::samples::samples',
//			asPopup: false,
//			maxWidth: 400,
//			maxHeight: 200,
//			minWidth: 150,
//			minHeight: 150,
//			relativePosition : {
//				relativeCompId: 'msorepeaterid'
//			}
//		}
//		appUtil.openApplication(appConfig);
//    },
//    
//    openMultiLevelList: function() {
//		var appUtil = new window['AppUtil']();
//		var appConfig = {
//			id : 'com::apporchid::system::serversidemultilevellistapp',
//			css:'ao-vux-entity-search-popup',
//			keyValueMap:{
//					ontologyName:"master"
//				},
//			size: {
//                width: 790,
//                height: screen.height * .7,
//            }
//		}
//		appUtil.openApplication(appConfig);
//	},
//	
//	openIconChooser: function () {
//    	var appUtil = new window['AppUtil']();
//	   var appConfig = {
//            id: 'com::apporchid::system::iconchooserapp',
//            size: { width: window.innerWidth * 0.8, height: window.innerHeight * 0.8 },
//            css: 'ao-vux-iconshooserapp',
//            showHeader: false,
//            padding: 0,
//        }
//       appUtil.openApplication(appConfig);
//    },
//    
//    openIconChooserWithButton: function () {
//    	var appUtil = new window['AppUtil']();
//	   var appConfig = {
//            id: 'com::apporchid::samples::iconChooserApp',
//            size: { width: window.innerWidth * 0.8, height: window.innerHeight * 0.8 },
//            css: 'ao-vux-iconshooserapp',
//            showHeader: false,
//            padding: 0,
//        }
//       appUtil.openApplication(appConfig);
//    }
	
	
};