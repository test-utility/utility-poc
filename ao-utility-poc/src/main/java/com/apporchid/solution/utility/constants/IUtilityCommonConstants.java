package com.apporchid.solution.utility.constants;

import com.apporchid.foundation.common.ICommonConstants;

public interface IUtilityCommonConstants {
	public static final String DEFAULT_DOMAIN_ID = ICommonConstants.ROOT_DOMAIN;
	public static final String DEFAULT_SUB_DOMAIN_ID = "utility";

}
