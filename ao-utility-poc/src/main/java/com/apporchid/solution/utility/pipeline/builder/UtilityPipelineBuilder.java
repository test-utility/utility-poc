package com.apporchid.solution.utility.pipeline.builder;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.apporchid.cloudseer.config.builder.BasePipelineConfigurationBuilder;
import com.apporchid.common.Variables;
import com.apporchid.foundation.common.IVariables;
import com.apporchid.foundation.pipeline.IPipeline;
import com.apporchid.foundation.pipeline.tasktype.ITaskType;
import com.apporchid.solution.utility.constants.IUtilityPipelineConstants;
@Component
public class UtilityPipelineBuilder  extends BasePipelineConfigurationBuilder implements IUtilityPipelineConstants{
	public UtilityPipelineBuilder() {
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}
	@Override
	protected List<IPipeline> getPipelines() {
		List<IPipeline> pipelinesList = new ArrayList<>();
		pipelinesList.add(createPipeline("BadgeIconPipeline", getExcelTasks("badge")));
		pipelinesList.add(createPipeline("ESAutoCompletePipeline", getESAutoCompletePipelineTasks()));
		
		///
		//pipelinesList.add(createPipeline(PIPELINE_ID_ES_AUTO_COMPLETE, getESAutoCompletePipelineTasks(AO_UTILITY_CUSTOMER_ES_INDEX)));
		//pipelinesList.add(createPipeline(AO_UTILITY_CUSTOMER_ES_DATA_LOAD_PIPELINE, getDBTOESDataLoadTasks()));
		return pipelinesList;
	}
	public ITaskType<?, ?>[] getESAutoCompletePipelineTasks() {
		return getESAutoCompletePipelineTasks(DEFAULT_ES_LOCAL_INDEX);
	}
	public ITaskType<?, ?>[] getESAutoCompletePipelineTasks(String indexName) {
		return esTaskBuilderHelper().getESAutoCompleteTasks(DEFAULT_ES_LOCAL_CLUSTER, indexName);
	}
	private ITaskType<?, ?>[] getExcelTasks(String fileName) {
		return getExcelTasks(fileName, null);
	}
	private ITaskType<?, ?>[] getExcelTasks(String fileName, Map<String, Object> additionalProps) {
		return fileTaskBuilderHelper().getExcelTasks("excel/" + fileName + ".xlsx", false, additionalProps);
	}
	
	@Override
	protected String[] getPipelinesToRun() {
		return new String[] { getSystemPipelineId(PIPELINE_NAME_EXCEL_BASED_ORDERED_MULIT_MSO_CREATOR),				
			//	getSystemPipelineId(PIPELINE_NAME_EXCEL_BASED_ORDERED_MULIT_MSO_CREATOR),
				getSystemPipelineId(PIPELINE_NAME_EXCEL_BASED_MSO_RELATIONSHIP_CREATOR)
				 };
	}

	@Override
	protected IVariables getInputVariables(String pipelineName, int index) {
		if (index == 0) {
			IVariables inputVariables = new Variables();
			inputVariables.add(VAR_ORDERED_MSO_FILE_PATH, "/utility/utility-all-msos.xlsx");
			return inputVariables;
		} 
		return null;
	}
}
