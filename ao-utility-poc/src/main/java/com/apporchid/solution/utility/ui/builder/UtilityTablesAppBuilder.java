package com.apporchid.solution.utility.ui.builder;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.apporchid.foundation.ui.config.application.IApplicationConfig;
import com.apporchid.solution.utility.constants.IUtilityPipelineConstants;
import com.apporchid.solution.utility.constants.IUtilitySolutionConstants;
import com.apporchid.vulcanux.config.builder.BaseAppConfigurationBuilder;

@Component
public class UtilityTablesAppBuilder  extends BaseAppConfigurationBuilder implements IUtilitySolutionConstants, IUtilityPipelineConstants{
	public UtilityTablesAppBuilder() {
		//call super with the default domain id and sub domain id so that all application that are created in this class are created by default with the default domain id and sub domain id
		super(DEFAULT_DOMAIN_ID, DEFAULT_SUB_DOMAIN_ID);
	}
	@Override
	protected List<IApplicationConfig> getApplications() {
		IApplicationConfig[] selectControlsApplications = new IApplicationConfig[] {
				
		};
		return Arrays.asList(selectControlsApplications);
	}
}
